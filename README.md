# README #

Code for an internet-connected plant pot, using an Arduino Yun as the sensor, and a DropWizard service to receive the readings and display a web page.

See LICENCE.md in this directory for licence information for the DropWizard service, and the arduino/LICENCE.md file for the Arduino licence.

## Building

### Arduino

Use the arduino/moistureSensorReading.ino file and upload to your Yun. You should use Arduino IDE 1.6.0 or later (earlier versions may not support the Yun).

### DropWizard

Needs Java 1.7+ and Maven 3.2+ to create an executable fatjar.

```bash
mvn package
```

## Running

### Arduino

Runs automatically when the code is uploaded from the Arduino IDE.

### DropWizard

```bash
java -jar target/SoilMoistureMonitor-1.0.0-SNAPSHOT.jar server
```

and visit http://localhost:8080/html to see the website.
