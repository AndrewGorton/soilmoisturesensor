package uk.andrewgorton.arduino.soilmoisturemonitor;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MeasurementsRepository {
    private static final Object LOCK_OBJECT = new Object();

    private static final Map<String, Measurement> measurements = new HashMap<String, Measurement>();

    public void MeasurementsRepository() {

    }

    public void addMeasurement(String sensor, Date when, Integer measurement) {
        Measurement tempMeasure = new Measurement();
        tempMeasure.setWhen(when);
        tempMeasure.setValue(measurement);

        synchronized (LOCK_OBJECT) {
            measurements.put(sensor, tempMeasure);
        }
    }

    public Map<String, Measurement> getMeasurements() {
        synchronized (LOCK_OBJECT) {
            Map<String, Measurement> clone = new HashMap<String, Measurement>();

            for (Map.Entry<String, Measurement> singleEntry : measurements.entrySet()) {
                Measurement tempMeasure = new Measurement();
                tempMeasure.setWhen(singleEntry.getValue().getWhen());
                tempMeasure.setValue(singleEntry.getValue().getValue());
                clone.put(singleEntry.getKey(), tempMeasure);
            }
            return clone;
        }

    }
}
