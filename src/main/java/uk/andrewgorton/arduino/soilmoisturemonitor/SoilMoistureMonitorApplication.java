package uk.andrewgorton.arduino.soilmoisturemonitor;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class SoilMoistureMonitorApplication extends Application<uk.andrewgorton.arduino.soilmoisturemonitor.configuration.SoilMoistureMonitorConfiguration> {
    public static void main(String[] args) throws Exception {
        new uk.andrewgorton.arduino.soilmoisturemonitor.SoilMoistureMonitorApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<uk.andrewgorton.arduino.soilmoisturemonitor.configuration.SoilMoistureMonitorConfiguration> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(uk.andrewgorton.arduino.soilmoisturemonitor.configuration.SoilMoistureMonitorConfiguration configuration,
                    Environment environment) {
        final uk.andrewgorton.arduino.soilmoisturemonitor.resources.SoilMoistureMonitorResource resource = new uk.andrewgorton.arduino.soilmoisturemonitor.resources.SoilMoistureMonitorResource();
        environment.jersey().register(resource);
    }
}
