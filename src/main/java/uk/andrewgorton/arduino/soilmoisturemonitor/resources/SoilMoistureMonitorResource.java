package uk.andrewgorton.arduino.soilmoisturemonitor.resources;

import com.codahale.metrics.annotation.Timed;
import uk.andrewgorton.arduino.soilmoisturemonitor.Measurement;
import uk.andrewgorton.arduino.soilmoisturemonitor.MeasurementsRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.Map;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class SoilMoistureMonitorResource {
    public SoilMoistureMonitorResource() {

    }

    @PUT
    @Path("/{sensor}/{date}/{reading}")
    @Timed
    public Response putTemperature(@PathParam("sensor") String sensor, @PathParam("date") Long when, @PathParam("reading") int sensorReading) {
        MeasurementsRepository mr = new MeasurementsRepository();
        mr.addMeasurement(sensor, new Date(when * 1000L), sensorReading);
        return Response.ok().build();
    }

    @GET
    @Timed
    public Response getReadings() {
        MeasurementsRepository mr = new MeasurementsRepository();
        Map<String, Measurement> measurements = mr.getMeasurements();
        return Response.ok(measurements).build();
    }

    @GET
    @Path("/html")
    @Produces(MediaType.TEXT_HTML)
    @Timed
    public Response getReadingsAsHtml() {
        MeasurementsRepository mr = new MeasurementsRepository();
        Map<String, Measurement> measurements = mr.getMeasurements();

        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html>");
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<style>");
        sb.append("table {\n" +
                "    border-collapse: collapse;\n" +
                "}\n" +
                "\n" +
                "table, th, td {\n" +
                "   border: 1px solid black;\n" +
                "} \n" +
                "\n" +
                "th, td {\n" +
                "    padding: 10px;\n" +
                "}\n" +
                "\n" +
                "td.green {\n" +
                "    background-color: green;" +
                "}\n" +
                "\n" +
                "td.yellow {\n" +
                "    background-color: yellow;" +
                "}\n" +
                "\n" +
                "td.red {\n" +
                "    background-color: red;" +
                "}\n" +
                "\n");
        sb.append("</style>");
        sb.append("</head>");
        sb.append("<body>");
        sb.append("<table>");
        sb.append("<tr><th>Sensor</th><th>Date</th><th>Value</th></tr>");
        for (Map.Entry<String, Measurement> singleSensor : measurements.entrySet()) {
            sb.append("<tr>");
            sb.append("<td>");
            sb.append(singleSensor.getKey());
            sb.append("</td>");
            sb.append("<td>");
            sb.append(singleSensor.getValue().getWhen().toString());
            sb.append("</td>");
            int reading = singleSensor.getValue().getValue();
            if (reading < 600) {
                sb.append("<td class=\"green\">");
            } else if (reading > 900) {
                sb.append("<td class=\"red\">");
            } else {
                sb.append("<td class=\"yellow\">");
            }
            sb.append(singleSensor.getValue().getValue());
            sb.append("</td>");
            sb.append("</tr>");
        }
        sb.append("</table>");
        sb.append("</body>");
        sb.append("</html>");

        return Response.ok(sb.toString()).build();
    }
}
