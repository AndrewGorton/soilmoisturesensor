package uk.andrewgorton.arduino.soilmoisturemonitor;

import java.util.Date;

public class Measurement {
    private Date when;
    private Integer value;
    
    public Measurement() {
        
    }

    public Date getWhen() {
        return when;
    }

    public void setWhen(Date when) {
        this.when = when;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
