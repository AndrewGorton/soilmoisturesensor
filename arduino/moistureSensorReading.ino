/**
 * Soil moisture monitor.
 *
 * Reads the analog soil moisture sensor and sends the data to a DropWizard
 * backend service.
 *
 * Uses an Arduino Yun - compiled with Arduino IDE v1.6.0.
 *
 * See LICENCE.md for licence details.
 *
 * tech@andrewgorton.uk
 */
#include <Bridge.h>

// Which analogue input pin is the AO of the moisture sensor attached to?
const int moistureSensorPin = 0;

// Which digital pins have which LEDs attached?
const int redLedPin = 2;
const int yellowLedPin = 4;
const int greenLedPin = 7;

// Delay in millseconds between reads
const byte readDelayMsec = 500;
const byte updateCurlIteration = 20; // Every 10 seconds, CURL the update

// 1023 => Dry
// 950 => In air
// 496 => In damp soil
// 244 => Both prongs fully submerged
// 0 => Wet
const int dryLevel = 900;
const int wetLevel = 600;

// This will hold the moisture reading
int moistureSensorReading = 0;
int readCount = 0;

void setup() {
  pinMode(redLedPin, OUTPUT);
  pinMode(yellowLedPin, OUTPUT);
  pinMode(greenLedPin, OUTPUT);
  digitalWrite(redLedPin, LOW);
  digitalWrite(yellowLedPin, LOW);
  digitalWrite(greenLedPin, LOW);

  Bridge.begin();

  Serial.begin(9600);
  Serial.println("Hi from your Arduino");
}

void loop() {
  delay(readDelayMsec);

  // Get the NTP sync'd time from the Linux side of the Yun
  Process time;
  time.runShellCommand("date +%s | tr -d '\n'");
  String timeString = "";
  while (time.available()) {
    char c = time.read();
    timeString += c;
  }
  Serial.println(timeString);

  // Read the soil sensor
  moistureSensorReading = analogRead(moistureSensorPin);
  readCount++;

  // Handy serial output for debugging
  Serial.print(timeString);
  Serial.print(" ");
  Serial.println(moistureSensorReading);

  // Update the on-board LEDs
  if (moistureSensorReading > dryLevel) {
    digitalWrite(redLedPin, HIGH);
    digitalWrite(yellowLedPin, LOW);
    digitalWrite(greenLedPin, LOW);
  } else if (moistureSensorReading < wetLevel) {
    digitalWrite(redLedPin, LOW);
    digitalWrite(yellowLedPin, LOW);
    digitalWrite(greenLedPin, HIGH);
  } else {
    digitalWrite(redLedPin, LOW);
    digitalWrite(yellowLedPin, HIGH);
    digitalWrite(greenLedPin, LOW);
  }

  // Put the results to the Bridge (if you want to access from the Linux side)
  Bridge.put(F("moisture1"), timeString + F(" ") + String(moistureSensorReading));
  // curl -XGET -uroot:arduino http://localhost/data/get/moisture1 && echo ""

  // Use curl on the Linux side to put the results to the DropWizard service
  if (readCount >= updateCurlIteration) {
    readCount = 0;
    String curlCommand = "curl -I -XPUT http://10.0.0.2:8080/moisture1/" + timeString + "/" + String(moistureSensorReading);
    Serial.println(curlCommand);
    Process curl;
    curl.runShellCommand(curlCommand);
    String curlResponseString;
    while (curl.available()) {
      char c = curl.read();
      curlResponseString += c;
    }
    Serial.println(curlResponseString);
  }
}
